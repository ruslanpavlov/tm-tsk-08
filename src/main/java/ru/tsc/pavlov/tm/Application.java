package ru.tsc.pavlov.tm;

import ru.tsc.pavlov.tm.api.ICommandRepository;
import ru.tsc.pavlov.tm.constant.ArgumentConst;
import ru.tsc.pavlov.tm.constant.TerminalConst;
//import ru.tsc.pavlov.tm.constant.util.NumberUtil;
import ru.tsc.pavlov.tm.model.Command;
import ru.tsc.pavlov.tm.repository.CommandRepository;
import ru.tsc.pavlov.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        showWelcome();
        parseArgs(args);
        process();
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                info();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                ShowErrorCommand();
        }
    }

    private static void ShowErrorArgument() {
        System.err.println("Error! Argument not supported.");
    }

    private static void ShowErrorCommand() {
        System.err.println("Error! Command not found.");
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                info();
                break;
            default:
                ShowErrorArgument();
        }
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Ruslan Pavlov");
        System.out.println("E-MAIL: o000.ruslan@yandex.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            showCommandValue(command.getName());
        }
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            showCommandValue(command.getArgument());
        }
    }

    private static void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    public static void process() {
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void info() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void exit() {
        System.exit(0);
    }

}
